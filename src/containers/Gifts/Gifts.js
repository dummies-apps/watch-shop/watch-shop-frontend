import React from "react";
import GiftsTemplate from "../../components/Gifts/Gifts";
import * as constants from "../../constants/constants";
import data from "../../mocks/gifts";
import { apiServer } from "../../config/axios";

class Gifts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null
    };
  }
  componentDidMount() {
    if (constants.USE_MOCKS) {
      this.setState({
        data: data
      });
    } else {
      apiServer
        .get(constants.GIFTS_API_URL)
        .then(response => {
          if (response.status === 200) {
            this.setState({
              data: response.data
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    }
  }
  render() {
    if (!this.state.data) return null;

    return <GiftsTemplate data={this.state.data} />;
  }
}

export default Gifts;
