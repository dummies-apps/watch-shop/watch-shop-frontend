import React from "react";
import { Route, Switch, withRouter } from "react-router-dom";
import Deals from "./containers/Deals/Deals";
import DashBoard from "./components/DashBoard/DashBoard";
import Gifts from "./containers/Gifts/Gifts";
import Sell from "./components/Sell/Sell";
import * as constants from "./constants/constants";

function App() {
  return (
    <Switch>
      <Route exact path="/" component={DashBoard} />
      <Route path={constants.DEALS_URL} component={Deals} />
      <Route path={constants.GIFTS_URL} component={Gifts} />
      <Route path={constants.SELL_URL} component={Sell} />
    </Switch>
  );
}

export default withRouter(App);
