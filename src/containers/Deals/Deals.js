import React from "react";
import DealsTemplate from "../../components/Deals/Deals";
import * as constants from "../../constants/constants";
import data from "../../mocks/deals";
import { apiServer } from "../../config/axios";

class Deals extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null
    };
  }
  componentDidMount() {
    if (constants.USE_MOCKS) {
      this.setState({
        data: data
      });
    } else {
      apiServer
        .get(constants.DEALS_API_URL)
        .then(response => {
          if (response.status === 200) {
            this.setState({
              data: response.data
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    }
  }
  render() {
    if (!this.state.data) return null;

    return <DealsTemplate data={this.state.data} />;
  }
}

export default Deals;
