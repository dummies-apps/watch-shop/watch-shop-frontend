import React from "react";
import Cards from "../Cards/Cards";
import data from "../../mocks/gifts";

const Gifts = () => (
  <div>
    <h3 style={{ textAlign: "center" }}>Gifts</h3>
    <Cards data={data} />
  </div>
);

export default Gifts;
