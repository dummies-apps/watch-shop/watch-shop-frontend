import React from "react";
import Cards from "../Cards/Cards";

const Deals = ({ data }) => (
  <div>
    <h3 style={{ textAlign: "center" }}>Deals</h3>
    <Cards data={data} />
  </div>
);

export default Deals;
