# Watch Shop FrontEnd

## Description

This is a sample application developed with react. It provides two routes :

- deals : which displays all watches in the shop
- gifts : which displays all the gift cards that you can buy and offer to someone

There's two modes to get the deals and gifts :

- mock : data is stored in json files
- api : data is provided by a remote server (as a rest web service)

To choose the mode, check the `constants/constants.js` file.

If you choose the api mode, you can use services provided by these projects :

-[Watch Shop HTTP](https://gitlab.com/dummies-apps/watch-shop/watch-shop-backend-http)

-[Watch Shop HTTPS](https://gitlab.com/dummies-apps/watch-shop/watch-shop-backend-https)

- Deals :

![deals](/uploads/5c55b12b6e9f0dc20453c5b349f8e932/deals.JPG)

- Gifts :

![gifts](/uploads/186870e50250ddf1f04d1355f43e3960/gifts.JPG)

## Requirements

For development, you will only need Node.js installed on your environment.

### Node

[Node](http://nodejs.org/) is really easy to install & now include [NPM](https://npmjs.org/).
You should be able to run the following command after the installation procedure
below.

    $ node --version
    v10.16.3

    $ npm --version
    6.9.0

#### Node installation on OS X

You will need to use a Terminal. On OS X, you can find the default terminal in
`/Applications/Utilities/Terminal.app`.

Please install [Homebrew](http://brew.sh/) if it's not already done with the following command.

    $ ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"

If everything when fine, you should run

    brew install node

#### Node installation on Linux

    sudo apt-get install python-software-properties
    sudo add-apt-repository ppa:chris-lea/node.js
    sudo apt-get update
    sudo apt-get install nodejs

#### Node installation on Windows

Just go on [official Node.js website](http://nodejs.org/) & grab the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it.

## Installation

    $ git clone https://gitlab.com/dummies-apps/watch-shop/watch-shop-frontend.git
    $ cd watch-shop-frontend
    $ npm install

### Start & watch

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### Simple build for production

    $ npm run build

## Docker Image

If you want to build the doker image after making some changes in the code use this :

    $ docker build -t mouhamedali/watch-shop-frontend .

Change the docker id and push the image to your docker hub. Otherwise, you can use mine :

    $ docker pull mouhamedali/watch-shop-frontend

You ca now run the app :

    $ docker run --rm --publish 3000:80 mouhamedali/watch-shop-frontend

Check this url : [http://localhost:3000](http://localhost:3000)

---

## Authors

- Mohamed Ali AMDOUNI
