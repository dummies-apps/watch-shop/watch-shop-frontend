import React from "react";
import classes from "./NavigationItems.css";
import NavigationItem from "./NavigationItem/NavigationItem";
import * as constants from "../../../constants/constants";

const NavigationItems = () => (
  <ul className={classes.NavigationItems}>
    <NavigationItem link={constants.DEALS_URL}>Deals</NavigationItem>
    <NavigationItem link={constants.GIFTS_URL}>Gift Cards</NavigationItem>
    <NavigationItem link={constants.SELL_URL}>Sell</NavigationItem>
  </ul>
);

export default NavigationItems;
