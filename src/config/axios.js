import axios from "axios";
import * as constants from "../constants/constants";

export const apiServer = axios.create({
  baseURL: constants.API_URL
});
