import React from "react";
import classes from "./Toolbar.css";
import NavigationItems from "../NavigationItems/NavigationItems";

const Toolbar = props => (
  <React.Fragment>
    <header className={classes.header}>
      <h1>Watch Shop</h1>
    </header>
    <nav>
      <NavigationItems />
    </nav>
  </React.Fragment>
);

export default Toolbar;
