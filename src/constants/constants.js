/*********************************************************/
// application config
/*********************************************************/

export const USE_MOCKS = true;

// if you choose to make a http request to get data, you have to mention the server url below and set USE_MOCKS to false
export const API_URL = "https://localhost:4300";
export const DEALS_API_URL = "/deals";
export const GIFTS_API_URL = "/gifts";

/*********************************************************/
// application routes
/*********************************************************/

export const BASE_URL = "/";
export const DEALS_URL = "/deals";
export const GIFTS_URL = "/gifts";
export const SELL_URL = "/sell";
