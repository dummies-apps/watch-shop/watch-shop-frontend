import React from "react";
import "./Cards.css";
import Card from "./Card/Card";

const Cards = props => {
  const { data } = props;

  const cards = data.map((cardInfo, index) => (
    <Card
      key={index}
      name={cardInfo.name}
      description={cardInfo.description}
      image={cardInfo.image}
      price={cardInfo.price}
      details={cardInfo.details}
    />
  ));

  return <div className="Cards">{cards}</div>;
};

export default Cards;
