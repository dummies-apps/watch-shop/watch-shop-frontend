import React from "react";
import "./Card.css";

const Card = ({ name, description, price, image, details }) => {
  return (
    <div className="Card">
      <div className="Image">
        <img src={image} alt={name} />
      </div>
      <div className="Info">
        <h3>{name}</h3>
        <ul className="Description">{description}</ul>
        <ol className="Details">
          {Object.keys(details).map((detail, index) => (
            <li key={index}>
              {detail} : {details[detail]}
            </li>
          ))}
        </ol>
      </div>
      <h3 style={{ textAlign: "center", marginTop: "10px", color: "#D2691E" }}>
        {price}
      </h3>
    </div>
  );
};

export default Card;
